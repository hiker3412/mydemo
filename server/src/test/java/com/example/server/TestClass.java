package com.example.server;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.alibaba.nacos.shaded.com.google.common.util.concurrent.RateLimiter;
import com.example.common.enumeration.ColorEnum;
import com.example.common.model.abstracttest.vo.User1;
import com.example.common.model.abstracttest.vo.User2;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class TestClass {

    @Test
    public void eq() {
        System.out.print(Double.parseDouble("123456"));
    }

    @Test
    public void regex() {
        String str = "[{\"title\":\"轮播图\",\"type\":\"banner\",\"componentContent\":{\"height\":270,\"bannerData\":[{\"bannerUrl\":\"http://192.168.110.197/file/2024/02/f29647872c7146569e8a322702ffbc71_轮播图竖版1.jpg\",\"linkObj\":{\"selsectValue\":\"\",\"selectName\":\"\",\"typeText\":\"\",\"url\":\"\"}},{\"title\":\"\",\"imgData\":\"\",\"url\":\"\",\"bannerUrl\":\"http://192.168.110.197/file/2024/02/dcca106b322b49a9b185533645340655_轮播图竖版2.jpg\"},{\"title\":\"\",\"imgData\":\"\",\"url\":\"\",\"bannerUrl\":\"http://192.168.110.197/file/2024/02/d1ec8b0cf7f74a2a89be9eae7eb7cdb2_轮播图竖版3.jpg\"},{\"title\":\"\",\"imgData\":\"\",\"url\":\"\",\"bannerUrl\":\"http://192.168.110.197/file/2024/02/c6142358d97342869c9b5b431aab935c_轮播图竖版4.jpg\"},{\"title\":\"\",\"imgData\":\"\",\"url\":\"\",\"bannerUrl\":\"http://192.168.110.197/file/2024/02/00996740305a40c3af8f9652a099dd06_轮播图竖版5.jpg\"}],\"hasDatas\":true},\"isEmpty\":false,\"index\":0},{\"title\":\"标题文本\",\"type\":\"text\",\"componentContent\":{\"title\":\"2312\",\"describe\":\"百度百科是百度公司推出的一部内容开放、自由的网络百科全书。其测试版于2006年4月20日上线，正式版在2008年4月21日发布，截至2023年4月，百度百科已经收录了超2700万个词条，参与词条编辑的网友超过770万人，几乎涵盖了所有已知的知识领域。\",\"textPos\":\"center\",\"fontSizeNum\":\"16\",\"describeSizeNum\":\"14\",\"textFontW\":\"bold\",\"describeFontW\":\"normal\",\"titColor\":\"#333333\",\"describeColor\":\"#666666\",\"bgColor\":\"#FFFFFF\",\"showLine\":true,\"showMore\":true,\"styleValue\":\"2\",\"link\":\"\",\"hasDatas\":true},\"isEmpty\":false,\"index\":1},{\"title\":\"品牌列表\",\"type\":\"brandList\",\"componentContent\":{\"title\":\"品牌列表\",\"imgList\":[{\"title\":\"标题一\",\"imgData\":\"http://192.168.110.197/file/2024/02/ed35e8783377427eba7c285e6e82378d_4586a830f959e60d70fc8f55ebcc295.png\",\"linkObj\":{\"selsectValue\":\"/detail\",\"selectName\":\"测试商品\",\"typeText\":\"商品\",\"data\":{\"shopId\":1,\"shopName\":\"北极心\",\"shopLogo\":\"\",\"productId\":1,\"skuId\":1,\"productName\":\"测试商品\",\"image\":\"http://192.168.110.197/file/2024/02/cb14c7b294654d3eadfa41e6b6c03ca9_开工大吉.png\",\"originalPrice\":10,\"price\":10,\"stockNumber\":1000,\"number\":0,\"users\":null,\"shopGroupWorkId\":null,\"shopSeckillId\":null,\"shopDiscountId\":null,\"ifEnable\":null,\"enableTime\":null,\"startTime\":\"\",\"endTime\":\"\",\"state\":null,\"time\":0}}},{\"title\":\"标题二\",\"imgData\":\"http://192.168.110.197/file/2024/02/81815ac7ec504b6386163685b7030926_组 9405.png\",\"linkObj\":{\"selsectValue\":\"/detail\",\"selectName\":\"测试商品\",\"typeText\":\"商品\",\"data\":{\"shopId\":1,\"shopName\":\"北极心\",\"shopLogo\":\"\",\"productId\":1,\"skuId\":1,\"productName\":\"测试商品\",\"image\":\"http://192.168.110.197/file/2024/02/cb14c7b294654d3eadfa41e6b6c03ca9_开工大吉.png\",\"originalPrice\":10,\"price\":10,\"stockNumber\":1000,\"number\":0,\"users\":null,\"shopGroupWorkId\":null,\"shopSeckillId\":null,\"shopDiscountId\":null,\"ifEnable\":null,\"enableTime\":null,\"startTime\":\"\",\"endTime\":\"\",\"state\":null,\"time\":0}}},{\"title\":\"标题三\",\"imgData\":\"http://192.168.110.197/file/2024/02/6c40dd611f934b84a11e07bb6f329c1a_组 9407.png\",\"linkObj\":{\"selsectValue\":\"/detail\",\"selectName\":\"测试商品\",\"typeText\":\"商品\",\"data\":{\"shopId\":1,\"shopName\":\"北极心\",\"shopLogo\":\"\",\"productId\":1,\"skuId\":1,\"productName\":\"测试商品\",\"image\":\"http://192.168.110.197/file/2024/02/cb14c7b294654d3eadfa41e6b6c03ca9_开工大吉.png\",\"originalPrice\":10,\"price\":10,\"stockNumber\":1000,\"number\":0,\"users\":null,\"shopGroupWorkId\":null,\"shopSeckillId\":null,\"shopDiscountId\":null,\"ifEnable\":null,\"enableTime\":null,\"startTime\":\"\",\"endTime\":\"\",\"state\":null,\"time\":0}}},{\"title\":\"标题四\",\"imgData\":\"http://192.168.110.197/file/2024/02/9affe0ca4b2c4d1299bdd2f536089c61_组 9408.png\",\"linkObj\":{\"selsectValue\":\"/detail\",\"selectName\":\"测试商品\",\"typeText\":\"商品\",\"data\":{\"shopId\":1,\"shopName\":\"北极心\",\"shopLogo\":\"\",\"productId\":1,\"skuId\":1,\"productName\":\"测试商品\",\"image\":\"http://192.168.110.197/file/2024/02/cb14c7b294654d3eadfa41e6b6c03ca9_开工大吉.png\",\"originalPrice\":10,\"price\":10,\"stockNumber\":1000,\"number\":0,\"users\":null,\"shopGroupWorkId\":null,\"shopSeckillId\":null,\"shopDiscountId\":null,\"ifEnable\":null,\"enableTime\":null,\"startTime\":\"\",\"endTime\":\"\",\"state\":null,\"time\":0}}}],\"textAlign\":\"left\",\"imgCurrent\":3,\"hasDatas\":true},\"isEmpty\":false,\"index\":3},{\"title\":\"公告\",\"type\":\"notice\",\"onlyAdmin\":true,\"componentContent\":{\"titColor\":\"#FFFFFF\",\"bgColor\":\"#FC810E\",\"hasDatas\":true},\"isEmpty\":false,\"index\":3},{\"title\":\"图文\",\"type\":\"imageText\",\"componentContent\":{\"imageUrl\":\"http://192.168.110.197/file/2024/02/a99a0853046e4c648914b0684dacd895_礼包专区.gif\",\"linkObj\":{\"selsectValue\":\"\",\"selectName\":\"\",\"typeText\":\"\",\"url\":\"\"},\"positionValue\":\"left\",\"title\":\"词条页\",\"content\":\"<p>\\t<a href=\\\"https://baike.baidu.com/item/%E8%AF%8D%E6%9D%A1/0?fromModule=lemma_inlink\\\" rel=\\\"noopener noreferrer\\\" target=\\\"_blank\\\" style=\\\"background-color: rgb(255, 255, 255); color: rgb(19, 110, 194);\\\">词条</a>\\t<span style=\\\"background-color: rgb(255, 255, 255);\\\">页主要由百科名片、正文内容和一些辅助的信息组成；百科名片包括概述和基本信息栏，其中概述为整个词条的简介，对全文进行概括性的介绍，基本信息栏主要是以表单的形式列出关键的信息点。</span></p>\",\"hasDatas\":true},\"isEmpty\":false,\"index\":4},{\"title\":\"图文列表\",\"type\":\"imageTextList\",\"componentContent\":{\"title\":\"标题\",\"textAlign\":\"left\",\"imgTextData\":[{\"linkObj\":{\"selsectValue\":\"\",\"selectName\":\"\",\"typeText\":\"\",\"url\":\"\"},\"isShow\":true,\"title\":\"图文标题\",\"describe\":\"告别生活，畅享便携生活\",\"imgData\":\"http://192.168.110.197/file/2024/02/4728f0d8511843e99dc1c33027a1d036_af4dc6ec69a7bcf7dcfb9eafa8cd050.png\"},{\"linkObj\":{\"selsectValue\":\"/detail\",\"selectName\":\"测试商品\",\"typeText\":\"商品\",\"data\":{\"shopId\":1,\"shopName\":\"北极心\",\"shopLogo\":\"\",\"productId\":1,\"skuId\":1,\"productName\":\"测试商品\",\"image\":\"http://192.168.110.197/file/2024/02/cb14c7b294654d3eadfa41e6b6c03ca9_开工大吉.png\",\"originalPrice\":10,\"price\":10,\"stockNumber\":1000,\"number\":0,\"users\":null,\"shopGroupWorkId\":null,\"shopSeckillId\":null,\"shopDiscountId\":null,\"ifEnable\":null,\"enableTime\":null,\"startTime\":\"\",\"endTime\":\"\",\"state\":null,\"time\":0}},\"isShow\":true,\"title\":\"图文标题图文标题\",\"text\":\"告别生活，畅享便携生活\",\"imgData\":\"http://192.168.110.197/file/2024/02/f6840e1f939f4b13b455ce4423830ef6_礼包专区.gif\",\"describe\":\"词条正文内容按照一定的结构对词条展开介绍，其中词条可以设置一级目\"},{\"linkObj\":{\"selsectValue\":\"/detail\",\"selectName\":\"测试商品\",\"typeText\":\"商品\",\"data\":{\"shopId\":1,\"shopName\":\"北极心\",\"shopLogo\":\"\",\"productId\":1,\"skuId\":1,\"productName\":\"测试商品\",\"image\":\"http://192.168.110.197/file/2024/02/cb14c7b294654d3eadfa41e6b6c03ca9_开工大吉.png\",\"originalPrice\":10,\"price\":10,\"stockNumber\":1000,\"number\":0,\"users\":null,\"shopGroupWorkId\":null,\"shopSeckillId\":null,\"shopDiscountId\":null,\"ifEnable\":null,\"enableTime\":null,\"startTime\":\"\",\"endTime\":\"\",\"state\":null,\"time\":0}},\"isShow\":true,\"title\":\"图文标题\",\"describe\":\"告别生活，畅享便携生活\",\"imgData\":\"http://192.168.110.197/file/2024/02/41dc8222c43a41aeb4b7a16a97c8d4bf_分享图.png\"}],\"hasDatas\":true},\"isEmpty\":false,\"index\":5},{\"title\":\"图文导航\",\"type\":\"imageTextNav\",\"componentContent\":{\"imgTextData\":[{\"linkObj\":{\"selsectValue\":\"/detail\",\"selectName\":\"测试商品\",\"typeText\":\"商品\",\"data\":{\"shopId\":1,\"shopName\":\"北极心\",\"shopLogo\":\"\",\"productId\":1,\"skuId\":1,\"productName\":\"测试商品\",\"image\":\"http://192.168.110.197/file/2024/02/cb14c7b294654d3eadfa41e6b6c03ca9_开工大吉.png\",\"originalPrice\":10,\"price\":10,\"stockNumber\":1000,\"number\":0,\"users\":null,\"shopGroupWorkId\":null,\"shopSeckillId\":null,\"shopDiscountId\":null,\"ifEnable\":null,\"enableTime\":null,\"startTime\":\"\",\"endTime\":\"\",\"state\":null,\"time\":0}},\"title\":\"测试1\",\"img\":\"http://192.168.110.197/file/2024/02/1e5f637ded3a49f68ada9875f606d597_402429129_2.gif\"},{\"linkObj\":{\"selsectValue\":\"/shop\",\"selectName\":\"北极心\",\"typeText\":\"店辅\",\"data\":{\"shopId\":1,\"shopName\":\"北极心\",\"phone\":\"17371815152\"}},\"title\":\"测试2\",\"img\":\"http://192.168.110.197/file/2024/02/da9cf0e0ab0c42aa8a567225700b33f1_402429129.gif\"},{\"linkObj\":{\"selsectValue\":\"/category\",\"selectName\":\"A1B\",\"typeText\":\"类别\",\"data\":{\"id\":2,\"parentId\":1,\"depth\":2,\"categoryName\":\"A1B\",\"classifyImage\":\"\",\"childs\":\"\"}},\"title\":\"测试3\",\"img\":\"http://192.168.110.197/file/2024/02/95fbed2108544795aee50d985b2e2e69_402429129_4.gif\"},{\"title\":\"测试4\",\"isShow\":true,\"imgData\":\"\",\"linkObj\":{\"selsectValue\":\"\",\"selectName\":\"\",\"typeText\":\"\",\"url\":\"\"},\"img\":\"http://192.168.110.197/file/2024/02/7c60df1cf0144c968434cc627e032417_402429129_3.gif\"}],\"hasDatas\":true},\"isEmpty\":false,\"index\":6},{\"title\":\"铺助分割\",\"type\":\"assistDiv\",\"componentContent\":{\"height\":7,\"bgColor\":\"#000000\",\"hasDatas\":true},\"isEmpty\":false,\"index\":7},{\"title\":\"视频\",\"type\":\"videoBox\",\"componentContent\":{\"title\":\"测试\",\"videoUrl\":\"http://cereshop.oss-cn-shenzhen.aliyuncs.com/0000/2020/05/432bb17e-536c-4762-a699-b3f5a2642742.mp4\",\"mainBody\":\"<p>\\t<span style=\\\"background-color: rgb(255, 255, 255);\\\">在词条正文底部，为参考资料以及开放分类信息，正文右侧的辅助信息包括词条统计、词条贡献榜、百科消息等，词条统计包含页面浏览和编辑次数、最近更新时间和创建者等信息；词条贡献榜突出显示对词条申请为</span>\\t<a href=\\\"https://baike.baidu.com/item/%E4%BC%98%E8%B4%A8%E7%89%88%E6%9C%AC/0?fromModule=lemma_inlink\\\" rel=\\\"noopener noreferrer\\\" target=\\\"_blank\\\" style=\\\"background-color: rgb(255, 255, 255); color: rgb(19, 110, 194);\\\">优质版本</a>\\t<span style=\\\"background-color: rgb(255, 255, 255);\\\">或</span>\\t<a href=\\\"https://baike.baidu.com/item/%E7%89%B9%E8%89%B2%E8%AF%8D%E6%9D%A1/0?fromModule=lemma_inlink\\\" rel=\\\"noopener noreferrer\\\" target=\\\"_blank\\\" style=\\\"background-color: rgb(255, 255, 255); color: rgb(19, 110, 194);\\\">特色词条</a>\\t<span style=\\\"background-color: rgb(255, 255, 255);\\\">的用户，并用勋章图标标记。</span></p>\",\"hasDatas\":true},\"isEmpty\":false,\"index\":8},{\"title\":\"自定义\",\"type\":\"custom\",\"componentContent\":{\"layoutType\":\"L1\",\"density\":\"4\",\"maxH\":0,\"imgClearance\":0,\"pageSpacing\":0,\"averageBoxData\":[],\"imgBoxActive\":0,\"elementNum\":1,\"imgData\":[{\"src\":\"http://192.168.110.197/file/2024/02/ed0c1dce655949b2b2cd1cae6f7d96c8_分享图.png\",\"linkObj\":{\"selsectValue\":\"\",\"selectName\":\"\",\"typeText\":\"\",\"url\":\"\"}},{\"src\":\"\",\"linkObj\":{\"selsectValue\":\"\",\"selectName\":\"\",\"typeText\":\"\",\"url\":\"\"}}],\"hasDatas\":true},\"isEmpty\":false,\"index\":9},{\"title\":\"自定义\",\"type\":\"custom\",\"componentContent\":{\"layoutType\":\"L2\",\"density\":\"4\",\"maxH\":0,\"imgClearance\":37,\"pageSpacing\":0,\"averageBoxData\":[],\"imgBoxActive\":1,\"elementNum\":2,\"imgData\":[{\"src\":\"http://192.168.110.197/file/2024/02/b43f974a75394d318063027fc0605a60_af4dc6ec69a7bcf7dcfb9eafa8cd050.png\",\"linkObj\":{\"selsectValue\":\"\",\"selectName\":\"\",\"typeText\":\"\",\"url\":\"\"}},{\"src\":\"http://192.168.110.197/file/2024/02/451279a1d3cf4dd5b4f2fea43c07c3f8_礼包专区.gif\",\"linkObj\":{\"selsectValue\":\"/detail\",\"selectName\":\"测试商品\",\"typeText\":\"商品\",\"data\":{\"shopId\":1,\"shopName\":\"北极心\",\"shopLogo\":\"\",\"productId\":1,\"skuId\":1,\"productName\":\"测试商品\",\"image\":\"http://192.168.110.197/file/2024/02/cb14c7b294654d3eadfa41e6b6c03ca9_开工大吉.png\",\"originalPrice\":10,\"price\":10,\"stockNumber\":1000,\"number\":0,\"users\":null,\"shopGroupWorkId\":null,\"shopSeckillId\":null,\"shopDiscountId\":null,\"ifEnable\":null,\"enableTime\":null,\"startTime\":\"\",\"endTime\":\"\",\"state\":null,\"time\":0}}}],\"hasDatas\":true},\"isEmpty\":false,\"index\":10},{\"title\":\"自定义\",\"type\":\"custom\",\"componentContent\":{\"layoutType\":\"L3\",\"density\":\"4\",\"maxH\":0,\"imgClearance\":27,\"pageSpacing\":28,\"averageBoxData\":[],\"imgBoxActive\":0,\"elementNum\":3,\"imgData\":[{\"src\":\"http://192.168.110.197/file/2024/02/c6c85cd3caf7478eb980764a5571e415_3e46a54f2d94b287f1f4aa2186aecf2.png\",\"linkObj\":{\"selsectValue\":\"/shop\",\"selectName\":\"北极心\",\"typeText\":\"店辅\",\"data\":{\"shopId\":1,\"shopName\":\"北极心\",\"phone\":\"17371815152\"}}},{\"src\":\"http://192.168.110.197/file/2024/02/475f29800b8a4e3cafb8834b1c83e152_6f6d9cc0e3e06188debe1d2b178d639.png\",\"linkObj\":{\"selsectValue\":\"\",\"selectName\":\"\",\"typeText\":\"\",\"url\":\"\"}},{\"src\":\"http://192.168.110.197/file/2024/02/89eba474f8c747cd844ca66753ba0aa6_a1b4f6088d8794b32b0c1f2c21480bf.png\",\"linkObj\":{\"selsectValue\":\"\",\"selectName\":\"\",\"typeText\":\"\",\"url\":\"\"}}],\"hasDatas\":true},\"isEmpty\":false,\"index\":11},{\"title\":\"自定义\",\"type\":\"custom\",\"componentContent\":{\"layoutType\":\"L4\",\"density\":\"4\",\"maxH\":0,\"imgClearance\":15,\"pageSpacing\":0,\"averageBoxData\":[],\"imgBoxActive\":3,\"elementNum\":4,\"imgData\":[{\"src\":\"http://192.168.110.197/file/2024/02/68e4982e0d024f6ab42b7eae40f5021b_402429129_2.gif\",\"linkObj\":{\"selsectValue\":\"\",\"selectName\":\"\",\"typeText\":\"\",\"url\":\"\"}},{\"src\":\"http://192.168.110.197/file/2024/02/e7c60c9fea924f41a9ec482b7ff929c4_402429129.gif\",\"linkObj\":{\"selsectValue\":\"\",\"selectName\":\"\",\"typeText\":\"\",\"url\":\"\"}},{\"src\":\"http://192.168.110.197/file/2024/02/aa443bd291104692aa8d7e2be2234590_402429129_4.gif\",\"linkObj\":{\"selsectValue\":\"\",\"selectName\":\"\",\"typeText\":\"\",\"url\":\"\"}},{\"src\":\"http://192.168.110.197/file/2024/02/c18ccfa4bad74b53aca1716cbfb8c970_402429129_3.gif\",\"linkObj\":{\"selsectValue\":\"\",\"selectName\":\"\",\"typeText\":\"\",\"url\":\"\"}}],\"hasDatas\":true},\"isEmpty\":false,\"index\":12},{\"title\":\"自定义\",\"type\":\"custom\",\"componentContent\":{\"layoutType\":\"T2B2\",\"density\":\"4\",\"maxH\":0,\"imgClearance\":21,\"pageSpacing\":100,\"averageBoxData\":[],\"imgBoxActive\":3,\"elementNum\":4,\"imgData\":[{\"src\":\"http://192.168.110.197/file/2024/02/da8f8d24e1b34899828d2f38fce1f359_402429129_2.gif\",\"linkObj\":{\"selsectValue\":\"\",\"selectName\":\"\",\"typeText\":\"\",\"url\":\"\"}},{\"src\":\"http://192.168.110.197/file/2024/02/e4baae3d659440d3ba12222eba12ba17_402429129.gif\",\"linkObj\":{\"selsectValue\":\"\",\"selectName\":\"\",\"typeText\":\"\",\"url\":\"\"}},{\"src\":\"http://192.168.110.197/file/2024/02/30faf61f1908446cab34fa26e2fa879f_402429129_4.gif\",\"linkObj\":{\"selsectValue\":\"\",\"selectName\":\"\",\"typeText\":\"\",\"url\":\"\"}},{\"src\":\"http://192.168.110.197/file/2024/02/e057d10e6a304127aa8d7d26229b457e_402429129_3.gif\",\"linkObj\":{\"selsectValue\":\"\",\"selectName\":\"\",\"typeText\":\"\",\"url\":\"\"}}],\"hasDatas\":true},\"isEmpty\":false,\"index\":13},{\"title\":\"自定义\",\"type\":\"custom\",\"componentContent\":{\"layoutType\":\"L1\",\"density\":\"4\",\"maxH\":0,\"imgClearance\":44,\"pageSpacing\":100,\"averageBoxData\":[],\"imgBoxActive\":0,\"elementNum\":1,\"imgData\":[{\"src\":\"\",\"linkObj\":{\"selsectValue\":\"\",\"selectName\":\"\",\"typeText\":\"\",\"url\":\"\"}}],\"hasDatas\":true},\"isEmpty\":true,\"index\":14}]";
        List<JSONObject> jsonObjects = JSONArray.parseArray(str, JSONObject.class);
        for (JSONObject obj : jsonObjects) {
            System.out.println(obj.getString("title"));
        }

    }

    public String formatStringWithVariableNames(String template, Map<String, Object> variables) {
        Pattern pattern = Pattern.compile("\\{([^}]*)}");
        StringBuffer sb = new StringBuffer();
        Matcher matcher = pattern.matcher(template);

        while (matcher.find()) {
            String varName = matcher.group(1);
            Object value = variables.get(varName);
            if (value != null) {
                matcher.appendReplacement(sb, Matcher.quoteReplacement(value.toString()));
            } else {
                throw new IllegalArgumentException("No value provided for variable: " + varName);
            }
        }
        matcher.appendTail(sb);

        return sb.toString();
    }

    @Test
    public void getStockStatus() {
        String input = "select * from trade where id = {myid} and name = {myname}";
        String regex = "\\{([^}]*)}";
        String s = "{}";
        Pattern pattern = Pattern.compile(regex);
        String[] matches = new String[10];
        Matcher matcher = pattern.matcher(input);

    }

    @Test
    public void we() {
        AtomicInteger i = new AtomicInteger(1);
        test10(i);
        System.out.println(i);

    }

    @Test
    public void test10(AtomicInteger i) {
        i.getAndIncrement();
    }


    @Test
    void testRateLimiter() {
        RateLimiter limiter = RateLimiter.create(10);
        int i = 1;
        while (i < 10) {
            i++;
            System.out.print("rate");
            System.out.println(limiter.getRate());
            limiter.acquire((int) limiter.getRate());
            System.out.println(LocalDateTime.now().getSecond());
        }
    }

    @Test
    void classPath() {
        System.out.println(Objects.requireNonNull(TestClass.class.getResource("/")));
    }

    @Test
    void netDownload2() {
        long until = LocalDateTime.now().until(LocalDateTime.of(LocalDate.now().plusDays(1), LocalTime.MIN), ChronoUnit.MINUTES);
        System.out.println(until);
    }

    @Test
    void netDownload3() throws Exception {
        URL url = new URL("http://news.windin.com/bulletin/79268996.pdf?mediatype=03&&pkid=79268996&&id=115467298");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        InputStream inputStream = conn.getInputStream();
        byte[] bytes = IOUtils.toByteArray(inputStream);
        System.out.println(bytes.length);
        FileOutputStream fileOutputStream = new FileOutputStream("C:\\Users\\Administrator\\IdeaProjects\\mydemo\\server\\src\\test\\resources\\test3.pdf");
        fileOutputStream.write(bytes);
    }

    @Test
    void testEnum() {
        System.out.println(ColorEnum.valueOf("RwED"));
    }

    @Test
    void printList() {
        ArrayList<String> strings = new ArrayList<>();
        strings.add("1");
        strings.add("1");
        strings.add("2");
        List<String> collect = strings.stream().distinct().collect(Collectors.toList());
        collect.forEach(System.out::println);
    }

    @Test
    void testAbstractField() {
        User1 user1 = new User1();
        User2 user2 = new User2();

        System.out.println(user2.getMyName());
    }

    @Test
    void testRegexp() {
        String etfConnection = ".+ETF联接[A_Z]*";
        System.out.println("上证180ETF联接".matches(etfConnection));
        System.out.println("上证180ETF联接A".matches(etfConnection));
        System.out.println("上证180ETF联接(LOF)".matches(etfConnection));
    }

    @Test
    void testLogic() {
        System.out.println(123);
        System.out.println(456);
    }

    @Test
    public void testHashCode() {
        ArrayList<String> strs = new ArrayList<>(Arrays.asList("002001.OF", "003001.SH", "515380.SZ"));
        strs.forEach(str -> System.out.println(str.hashCode()));
        strs.forEach(str -> System.out.println(str.hashCode() % 4));
    }

    @Test
    public Double divide(Double... v) {
        Double result = null;
        for (Double aDouble : v) {
            if (aDouble == null || aDouble == 0) return null;
            if (result == null) {
                result = aDouble;
            } else {
                result = result / aDouble;
            }
        }
        return result;
    }
}

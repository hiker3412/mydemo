package com.example.server.controller;

import com.alibaba.nacos.shaded.com.google.common.util.concurrent.RateLimiter;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.File;
import java.io.FileInputStream;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Controller
public class FileController {

    private final RateLimiter limiter = RateLimiter.create(102400.0);

    @RequestMapping("/download")
    void download(HttpServletResponse response) throws Exception {
        LocalDateTime startDate = LocalDateTime.now();
        long start = startDate.toInstant(ZoneOffset.of("+8")).toEpochMilli();

        String path = "C:\\Users\\Administrator\\IdeaProjects\\mydemo\\server\\src\\main\\resources\\static\\file\\阿里巴巴Java开发手册.pdf";
        File file = new File(path);
        FileInputStream fileInputStream = new FileInputStream(file);
        ServletOutputStream outputStream = response.getOutputStream();
        byte[] bytes = new byte[1024];
        int redLen = 0;
        while ((redLen = fileInputStream.read(bytes)) > 0) {
            limiter.acquire(redLen);
            outputStream.write(bytes);
        }

        LocalDateTime endDate = LocalDateTime.now();
        long end = endDate.toInstant(ZoneOffset.of("+8")).toEpochMilli();

        System.out.println("file length" + file.length());
        System.out.println("available" + fileInputStream.available());
        System.out.println("spend time" + (end - start));
    }
}

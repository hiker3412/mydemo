package com.example.server.controller;

import com.example.common.model.FormatVo;
import com.example.common.util.HttpUtils;
import com.example.server.feign.server2.TestClient;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import feign.Request;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping("/test")
@Slf4j
public class MyRestController {

    LoadingCache cache = CacheBuilder.newBuilder()
            .expireAfterWrite(5,TimeUnit.SECONDS)
            .build(new CacheLoader<Object, Object>() {
                @Override
                public Object load(Object o) throws Exception {
                    System.out.println("loading============================");
                    return new Date();
                }
            });

    @Resource
    private TestClient testClient;
    @Resource
    private Gson gson;

    @RequestMapping("/exception")
    public String exception() throws Exception {
        System.out.println("sout 测试异常");
        log.error("log 测试异常");
        throw new Exception("测试异常");
    }

    @RequestMapping("/guavaCache")
    public Object guavaCache(){
        return cache.getUnchecked("");
    }

    @RequestMapping("/jsonP")
    public Object jsonP(String callback){
        Map<String,Object> map = new HashMap<>();
        map.put("name","Jack");
        map.put("age",25);

        Type type = new TypeToken<Map<String,Object>>(){}.getType();
        String json = gson.toJson(map, type);
        return callback + "(" + json + ")";
    }

    @GetMapping("/format")
    public Object format(double v){
        return new FormatVo().setD1(v);
    }

    @GetMapping("/path1/**")
    public Object pathVariable(HttpServletRequest request){

        String requestURI = request.getRequestURI();
        int path1Index = requestURI.indexOf("path1");
        return requestURI.substring(path1Index + "path1".length() + 1);
    }

    @GetMapping("/arrayParam")
    public Object arrayParam(ArrayList<String> aList){
        log.info("收到的参数是：");
        aList.forEach(System.out::println);
        return aList;
    }

    @GetMapping("/feign")
    public String feign(String clientId){
        switch (clientId){
            case "3" :
                return testClient.sleep3(new String[]{"3"});
            case "6":
                return testClient.sleep6(new String[]{"6"},new Request.Options(2, TimeUnit.SECONDS,60,TimeUnit.SECONDS,true));
            default:
                return "0";
        }
    }
}

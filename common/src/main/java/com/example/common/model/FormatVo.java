package com.example.common.model;

import com.example.common.serialize.P2;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.NumberFormat;

@Data
@Accessors(chain = true)
public class FormatVo {
    @JsonSerialize(using = P2.class)
    private Double d1;

}

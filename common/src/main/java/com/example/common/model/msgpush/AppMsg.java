package com.example.common.model.msgpush;

import java.util.List;

/**
 * 统一消息推送对象，由于各平台支持的特性不一样，这里只包括共有特性，
 * 个性化特性新建类继承这个类，例如HuaweiAppMsg extends AppMsg
 */
public class AppMsg {
    public static String APP_GJ = "fm";
    public static String APP_HQT = "hqt";
    public static String PUSH_SCOPE_ALL = "pushAll";
    public static String PUSH_SCOPE_GROUP = "pushGroup";
    public static String PUSH_SCOPE_BANK_CHANNEL = "pushBankChannel";

    /**
     * 要推送的目标app，{@link #APP_GJ#APP_HQT}
     */
    private String appType;
    /**
     * 要推送的目标app版本
     */
    private String appVersion;
    /**
     * 推送范围,{@link #PUSH_SCOPE_ALL#PUSH_SCOPE_GROUP#PUSH_SCOPE_BANK_CHANNEL}
     */
    private String pushScope;
    /**
     * 推送范围对应的id,
     */
    private List<String> pushScopeIds;
    /**
     * 消息id
     */
    private String msgId;
    /**
     * 状态栏标题
     */
    private String title;
    /**
     * 状态栏副标题
     */
    private String subtitle;

}
